#include <SFML/Graphics.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/VideoMode.hpp>

#include <iostream>
#include "objects.hpp"

#define Width 660
#define Height 440

int main(void) {

	sf::RenderWindow game(sf::VideoMode(660, 440), "Physics");


	Object shape(sf::CircleShape(24, 7), PhysicsOptions(), sf::Vector2f(330, 0),[](Object * x){
		auto pos = x->shape.getPosition();

		if (pos.y > 440) { x->shape.setPosition(pos.x, 0); } 
		
		//cout << x->velocity << endl;
		//x->shape.rotate(1);
		//x->shape.setRadius(x->shape.getRadius()+1);
	});
	
	
	game.setFramerateLimit(60);

	while (game.isOpen()) {

		sf::Event event;
		
		while (game.pollEvent(event)) {
			if (event.type == sf::Event::Closed) { game.close(); }
			if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Escape) { game.close(); }
		}

		game.clear(sf::Color(135, 206, 235, 255));

		
		shape.update();
		game.draw(shape.shape);
		
		game.display();
		
	}
	return 0;
}
