#include <SFML/Graphics.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Window.hpp>

#include <cstddef>
#include <string>
#include <functional>

using namespace std;

const float DEFAULTGRAVITY = 0.01;
typedef sf::CircleShape Shape;


/*
	Physics Options.
	Contains options for the physics of the `Object` class.
*/
class PhysicsOptions {

public:
	float gravity;
	float mass;
	bool gravityEnabled;
	
	PhysicsOptions(float gravity=DEFAULTGRAVITY, float mass=10, bool gravityEnabled=true){
		this->gravity = gravity;
		this->mass = mass;
		this->gravityEnabled = gravityEnabled;
	}
};

/*
	A class that includes `sf::CircleShape` and a float variable called velocity.
	Includes a private lambda called `loop` that gets ran after using the `update` method.
*/
class Object {

	PhysicsOptions options;
	function<void(Object* x)> loop = [](Object* x){};
	
public:

	float velocity=0;
	Shape shape;
	
	Object(Shape shape, PhysicsOptions options, sf::Vector2f position,function<void(Object* x)> loop=[](Object * x){}) {
		this->shape = shape;
		this->options = options;
		this->loop = loop;

		this->shape.setPosition(position);
	}

	void update() {
		auto pos = this->shape.getPosition();

		if (this->options.gravityEnabled) {
			this->velocity += (this->options.gravity * ((this->shape.getRadius()) / this->options.mass));
			this->shape.setPosition(pos.x, pos.y+this->velocity);
		}

		this->loop(this);
	}
};
