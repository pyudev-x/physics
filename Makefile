CC=g++
MAIN=main.cpp
OUTPUT=build
MSG=Compile Info:

compile:
	echo $(MSG) Started Compiling
	if [ ! -d $(OUTPUT) ]; then mkdir $(OUTPUT); fi
	$(CC) $(MAIN) -o $(OUTPUT)/app -lsfml-graphics -lsfml-window -lsfml-system
	echo $(MSG) Done Compiling
run:
	./$(OUTPUT)/app