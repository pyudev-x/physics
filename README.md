# physics

Some random gravity thingy I made using C++ and SFML.

Feel free to use `objects.hpp` for your own SFML projects!

## Requirements

* SFML
* GNU Compiler Collection (GCC)

## Compiling

Download the source code and simply open a terminal in the directory of the source code and run `make compile -s` (The -s option is optional)

To run the program you can run `make run -s` (The -s option is optional)